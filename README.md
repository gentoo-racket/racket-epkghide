# EPKGHide

<p align="center">
    <a href="https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.com/gentoo-racket/racket-epkghide">
        <img src="https://archive.softwareheritage.org/badge/origin/https://gitlab.com/gentoo-racket/racket-epkghide/">
    </a>
    <a href="https://gitlab.com/gentoo-racket/racket-epkghide/pipelines">
        <img src="https://gitlab.com/gentoo-racket/racket-epkghide/badges/master/pipeline.svg">
    </a>
</p>

Hide installed packages.


## About

This project is inpired by Alfredfo's
[ehide](https://github.com/alfredfo/ehide/) program.


## Requirements

Portage does not work correctly in bubblewrap sandbox without root privileges.
You either have to set `USE=suid` for `sys-apps/bubblewrap`
or run `epkghide` on root account (or with `su`/`sudo`).


## License

Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>

Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later
