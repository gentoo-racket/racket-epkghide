;; This file is part of racket-epkghide - hide installed packages.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-epkghide is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-epkghide is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-epkghide.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require ziptie/git/hash
          epkghide/version)


@(define (in-upstream path)
   (format "https://gitlab.com/gentoo-racket/racket-eclass2scrbl/-/tree/~a/"
           path))


@title[#:tag "epkghide"]{EPKGHide}

@author[@author+email["Maciej Barć" "xgqt@riseup.net"]]


Hide installed packages.

Version: @link[@in-upstream[@VERSION]]{@VERSION},
commit hash:
@(let ([git-hash (git-get-hash #:short? #t)])
   (case git-hash
     [("N/A")
      (displayln "[WARNING] Not inside a git repository!")
      git-hash]
     [else
      (link (in-upstream git-hash) git-hash)]))


@table-of-contents[]


@index-section[]
