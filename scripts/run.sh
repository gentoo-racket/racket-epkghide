#!/bin/sh


# This file is part of racket-epkghide - hide installed packages.
# Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v2 License
# SPDX-License-Identifier: GPL-2.0-or-later

# racket-epkghide is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# racket-epkghide is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with racket-epkghide.  If not, see <https://www.gnu.org/licenses/>.


set -e
trap 'exit 128' INT
export PATH


root_source="$(realpath "$(dirname "${0}")/../")"


exec racket "${root_source}"/epkghide/main.rkt "${@}"
